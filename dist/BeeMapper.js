// Handle the layer creation and stuff
var wms_sources = [];
var wms_layers = [];
var toggle_layers = [];
var layers = [];

// WMS sources
var sources = [
	'LandCover',
	'OverallAbund',
	'LandCover',
	'OverallAbund',
	'Lakes',
	'Rivers / Streams',
	'Towns',
	'Roads',
];

// Define an invisible layer for highlighting missing data
var source_interaction = new ol.source.Vector();
var layer_interaction = new ol.layer.Vector({
	source: source_interaction,
	style: new ol.style.Style({
		fill: new ol.style.Fill({
			color: 'rgba(150, 150, 150, 0.2)'
		}),
		stroke: new ol.style.Stroke({
			color: '#ffcc33',
			width: 2
		}),
		image: new ol.style.Circle({
			radius: 7,
			fill: new ol.style.Fill({
				color: '#ffcc33'
			})
		})
	})
});

// Define sources for the shape files
var fieldSource = new ol.source.Vector({
	url: './maps/fields.json',
	format: new ol.format.GeoJSON()
});

var nearSource = new ol.source.Vector({
	url: './maps/250Buffer.json',
	format: new ol.format.GeoJSON()
});

var farSource = new ol.source.Vector({
	url: './maps/1KBuffer.json',
	format: new ol.format.GeoJSON()
});

// Define styles for the vector layers
var clearStyle = new ol.style.Style({
	fill: new ol.style.Fill({
		color: 'rgba(0,0,0,0)',
	})
});

var fieldStyle = new ol.style.Style({
	fill: new ol.style.Fill({
			color: 'rgba(0, 0, 240, 0.8)',
	})
});

var mouseStyle = new ol.style.Style({
    fill: new ol.style.Fill({
        color: 'rgba(255,179,0, 0.8)',
    }),
});

var nearStyle = [new ol.style.Style({
	fill: new ol.style.Fill({
			color:'rgba(0,0,120,0.05)', 
	}),
	stroke: new ol.style.Stroke({color: 'black', width: 1})
})];

var farStyle = [new ol.style.Style({
	fill: new ol.style.Fill({
			color:'rgba(0,0,120,0.1)', 
			opacity: 0.75
	}),
	stroke: new ol.style.Stroke({color: 'black', width: 1})
})];

wms_layers.push(new ol.layer.Tile({
	title: 'Sat',
	visible: false,
	preload: Infinity,
	source: new ol.source.BingMaps({
		key: 'AqRY7vLav9l1mIc1iGuVDZEw6Ju6BFS8YSrL6haLdYin7nmooM6hlH_uKQHB5XmV',
		imagerySet: ['Aerial'], 
		maxZoom: 16
	}),
}));

wms_layers.push(new ol.layer.Tile({
	title: 'Road',
	visible: true,
	preload: Infinity,
	source: new ol.source.BingMaps({
		key: 'AqRY7vLav9l1mIc1iGuVDZEw6Ju6BFS8YSrL6haLdYin7nmooM6hlH_uKQHB5XmV',
		imagerySet: ['Road'], 
		maxZoom: 16
	}),
}));

// Add the raster data from WMS
for (var i = 0, len = sources.length; i < len; i++) {

	var url = 'https://beemapper-wfcb.acg.umaine.edu/mapproxy/service?';
	var format = 'image/png';

	if (i >= 2) {
		wms_sources.push(
			new ol.source.TileWMS({
				url: 'https://beemapper-wfcb.acg.umaine.edu/mapproxy/service',
				params: {
					LAYERS: sources[i],
					VERSION: '1.1.1'
				}
			})
		);
		wms_layers.push(
			new ol.layer.Tile({
				source: wms_sources[i],
				title: sources[i],
				preload: Infinity,
			})
		);

	}
	else {

		wms_sources.push(
			new ol.source.ImageWMS({
					url: url,
					params: {
						'LAYERS': sources[i],
						'FORMAT': format,
					}
			})
		);

		wms_layers.push(
			new ol.layer.Image({
					title: sources[i],
					preload: Infinity,
					source: wms_sources[i],
			})
		);
	}


	// Don't make the Abund visible to start
	if (i == 2 || i == 3 | i == 4 | i == 5) {
		wms_layers[i].setVisible(false);
	}
}

var satellite_layer = wms_layers[0];
var road_layer = wms_layers[1];
var landcover_layer = wms_layers[4];
var abundance_layer = wms_layers[5];

layers.push(
		new ol.layer.Vector({
			title: '1000M',
			style: clearStyle,
			source: farSource
		})
);

layers.push(
		new ol.layer.Vector({
			title: '250M',
			style: clearStyle,			
			source: nearSource
	})
);

layers.push(
		new ol.layer.Vector({
			title: 'Fields',
			style: fieldStyle,
			source: fieldSource
		})
);

layers.push( layer_interaction );
;// Define a search box on the map
var searchBox = function(opt_options) {
	var options = opt_options || {};
	var data = {};
	var template = $('#search-template').html();
	var element = document.createElement('div');
	element.innerHTML = Mustache.render(template, data);

	ol.control.Control.call(this, {
		element: element,
		target: options.target
	});
};
ol.inherits(searchBox, ol.control.Control);

// Define a search box on the map
var feedBack = function(opt_options) {
	var options = opt_options || {};
	var data = {};
	var template = $('#feedback').html();
	var element = document.createElement('div');
	element.innerHTML = Mustache.render(template, data);


	ol.control.Control.call(this, {
		element: element,
		target: options.target
	});
};
ol.inherits(feedBack, ol.control.Control);

// Define a box on the map to hold area info text
var info = function(opt_options) {
	var options = opt_options || {};
	var data = {};

	var template = $('#info-box').html();
	var element = document.createElement('div');
	element.innerHTML = Mustache.render(template, data);

	ol.control.Control.call(this, {
		element: element,
		target: options.target
	});
};
ol.inherits(info, ol.control.Control);

// Define a button on the map to toggle the Legend
var legendToggle = function(opt_options) {
	var options = opt_options || {};
	var data = {
		'div-id': 'legend',
		'b-title': 'Click to show legend',
		'b-class': 'toggle',
		'b-id': '',
		'text': 'Legend'
	};
	var template = $('#map-button').html();
	var element = document.createElement('div');
	element.innerHTML = Mustache.render(template, data);

	ol.control.Control.call(this, {
		element: element,
		target: options.target
	});
};

ol.inherits(legendToggle, ol.control.Control);

// Define a box on the map to hold area legend image
var legendBox = function(opt_options) {
	var options = opt_options || {};
	var data = {
		'd-class': 'ol-legendControl',
		'd-id': 'legendBox',
		'dd-id': 'legendMap',
	};
	var template = $('#map-box').html();
	var element = document.createElement('div');
	element.innerHTML = Mustache.render(template, data);

	ol.control.Control.call(this, {
		element: element,
		target: options.target
	});
};

ol.inherits(legendBox, ol.control.Control);

// Define a box on the map to hold area legend image
var layerBox = function(opt_options) {
	var options = opt_options || {};
	var data = {
		'd-class': 'ol-layerBox',
		'd-id': 'layerBox',
		'dd-id': 'layerToggles',
	};
	var template = $('#map-box').html();
	var element = document.createElement('div');
	element.innerHTML = Mustache.render(template, data);

	ol.control.Control.call(this, {
		element: element,
		target: options.target
	});
};
ol.inherits(layerBox, ol.control.Control);

var LayerToggle = function(opt_options) {
	var options = opt_options || {};
	var data = {
		'div-id': 'LayerToggleDiv',
		'b-title': 'Show additional layers',
		'b-class': '',
		'b-id': 'LayerToggleButton',
		'text': 'Layers'
	};
	var template = $('#map-button').html();
	var element = document.createElement('div');
	element.innerHTML = Mustache.render(template, data);

	ol.control.Control.call(this, {
		element: element,
		target: options.target
	});
};
ol.inherits(LayerToggle, ol.control.Control);

var abundanceToggle = function(opt_options) {
	var options = opt_options || {};
	var data = {
		'div-id': 'AbundanceToggle',
		'div-value': 1,
		'b-title': 'Toggle abundance layer',
		'b-id': 'abundanceToggleButton',
		'b-val': 1,
		'text': 'Abundance',
		'idiv-class': 'sliderDiv',
		'idiv-id': 'abundanceSliderDiv',
		'input-title': 'Adjust Map Transparency',
		'input-class': 'layerSlider',
		'input-name': '5',
	};
	var template = $('#layer-toggle').html();
	var element = document.createElement('div');
	element.innerHTML = Mustache.render(template, data);

	ol.control.Control.call(this, {
		element: element,
		target: options.target
	});
};
ol.inherits(abundanceToggle, ol.control.Control);

var landToggle = function(opt_options) {
	var options = opt_options || {};
	var data = {
		'div-id': 'LandToggle',
		'div-value': 1,
		'b-title': 'Toggle land cover layer',
		'b-id': 'landToggleButton',
		'b-val': 1,
		'text': 'Land Cover',
		'idiv-class': 'sliderDiv',
		'idiv-id': 'landSliderDiv',
		'input-title': 'Adjust Map Transparency',
		'input-class': 'layerSlider',
		'input-name': '4',
	};
	var template = $('#layer-toggle').html();
	var element = document.createElement('div');
	element.innerHTML = Mustache.render(template, data);

	ol.control.Control.call(this, {
		element: element,
		target: options.target
	});
};
ol.inherits(landToggle, ol.control.Control);

var satToggle = function(opt_options) {
	var options = opt_options || {};
	var data = {
		'div-id': 'satToggle',
		'b-title': 'Toggle satellite base layer',
		'b-class': '',
		'b-id': 'satToggleButton',
		'text': 'Satellite'
	};
	var template = $('#map-button').html();
	var element = document.createElement('div');
	element.innerHTML = Mustache.render(template, data);

	ol.control.Control.call(this, {
		element: element,
		target: options.target
	});
};
ol.inherits(satToggle, ol.control.Control);

var roadToggle = function(opt_options) {
	var options = opt_options || {};
	var data = {
		'div-id': 'roadToggle',
		'b-title': 'Toggle road base layer',
		'b-class': '',
		'b-id': 'roadToggleButton',
		'text': 'Road'
	};
	var template = $('#map-button').html();
	var element = document.createElement('div');
	element.innerHTML = Mustache.render(template, data);

	ol.control.Control.call(this, {
		element: element,
		target: options.target
	});
};
ol.inherits(roadToggle, ol.control.Control);

var ZoomToExtent = function(opt_options) {
	var options = opt_options || {};
	var data = {
		'div-id': 'ZoomToExtent',
		'b-title': 'Zoom to map extent',
		'b-class': '',
		'b-id': 'ZoomExtent',
		'text': 'R'
	};
	var template = $('#map-button').html();
	var element = document.createElement('div');
	element.innerHTML = Mustache.render(template, data);

	ol.control.Control.call(this, {
		element: element,
		target: options.target
	});
};

ol.inherits(ZoomToExtent, ol.control.ZoomToExtent);

// Setup the map object
var mapView = new ol.View({
	center: [-7595180.242828147,5558499.882142702],
	zoom: 10,
	minZoom: 9,
	maxZoom: 17 
});

var container = document.getElementById('popup');
var content = document.getElementById('popup-content');
var closer = document.getElementById('popup-closer');

closer.onclick = function() {
	overlay.setPosition(undefined);
	closer.blur();
	return false;
};

var overlay = new ol.Overlay({
	element: container
});

// Actually create the map
var map = new ol.Map({
	controls: ol.control.defaults({
	}).extend([
		new info(),
		new LayerToggle(),
		new layerBox(),
		new legendToggle(),
		new legendBox(),
		new searchBox(),
		new ZoomToExtent(),
		new abundanceToggle(),
		new landToggle(),
		new roadToggle(),
		new satToggle(),
		new ol.control.ScaleLine({'units': 'us'}),
		new feedBack(),
	]),
	layers: wms_layers.concat(layers),
	target: 'map',
	overlays: [overlay],
	view: mapView
});
;// Some Globals
$(".ol-attribution button").html('?');

var draw = new ol.interaction.Draw({
	source: source_interaction,
	type: "Polygon",
});

var drawing = false;

var fields_visible = true;
var selected_buffers = [];
var field_averages = null;
var field_info = null;

var landCoverLegend = [
				['Deciduous Edge', '#FAE925', 'The transition between forest with hardwood or a mixture of hardwood and softwood trees and another land cover type. Provides very high quality bee habitat.'],
				['Developed', '#EC2023', 'Any kind of development, including roads, towns, and cities. Provides high quality bee habitat.'],
				['Coniferous Forest', '#3A8A2C', 'Consists of softwood trees. Provides poor bee habitat.'],
				['Deciduous Forest', '#59E333', 'Consists of hardwood or a mixture of hardwood and softwood trees. Provides moderately high quality bee habitat.'],
				['Emergent Wetland', '#AC0EF5' , 'Wetland with exposed soil, wildflowers, and woody flowering shrubs. Provides moderate quality bee habitat.'],
				['Wetlands / Water', '#878787', 'Either submerged wetlands with few flowering plants or open water. Provides moderately poor bee habitat.'],
				['Agriculture', '#FF8800', 'Either non-blueberry agriculture or open, grassy fields. Provides moderate quality bee habitat.'],
				['Blueberries', '#2B83BA', 'Consists of blueberry fields that may be managed or unmanaged. Provides moderate quality bee habitat.']
			];

var abundanceLegend = [
				['Low', '#FFFFD4', 'Approximately 0.1 bees per square yard per minute. Estimated contribution to fruit set is 12%.'],
				['Low / Medium', '#FED98E', 'Approximately 0.2 bees per square yard per minute. Estimated contribution to  fruit set is 18%.'],
				['Medium', '#FE9929', 'Approximately 0.3 bees per square yard per minute. Estimated contribution to fruitset is 20%.'],
				['Medium / High', '#D95F0E', 'Approximately 0.4 bees per square yard per minute. Estimated contribution to fruit set is 25%.'],
				['High', '#993404', 'Approximately 0.5-1.0 bees per square yard per minute. Estimated contribution to fruit set is 30%.']
			];


function drawLegend(legend, title, name) {

	var container = document.createElement('div');
	container.id = name;
	container.className += ' legendImage';

	var table = document.createElement('table');
	table.className = 'legendTable';
	var tbody = document.createElement('tbody');

	for (var i = 0; i < legend.length; i++) {
		var row = document.createElement('tr');
		row.className = 'vertical-spacing';

		var layer = document.createElement('td');
		layer.className = 'legendTitle';
		var color = document.createElement('td');
		var div = document.createElement('div');
		div.className = "colorBox";
		color.title = legend[i][2];
		color.className = 'tipster';

		color.appendChild(div);

		layer.appendChild(document.createTextNode(legend[i][0]));
		div.style.backgroundColor = legend[i][1];

		row.appendChild(color);
		row.appendChild(layer);
		tbody.appendChild(row);
	}
	table.appendChild(tbody);
	container.appendChild(table);
	container.style.display = 'none';
	return container;
}

// Load in field information
$.getJSON("assets/summary.json", function(json) {
	field_info = json;
});

function translateLand(value) {
    console.log(value);
	if (value === 0) {
		return "No Data";
	}
	else {
		return landCoverLegend[value - 1][0];	
	}
}

function translateDensity(value) {

	// TODO Automate this in some way!
	if (value == 5) {
		return 'High';
	}
	else if (value == 4) {
		return 'Medium/High';
	}
	else if (value == 3) {
		return 'Medium';
	}
	else if (value == 2) {
		return 'Low/Medium';
	}
	else {
		return 'Low';
	}
}

// Global list of selected features
var selected_features = [];

function map_click (evt) {

	// Get any features on the clicked area
	var fields = fieldSource.getFeaturesAtCoordinate(evt.coordinate);
	
	// Highlight or clear selected fields
	color_features(fields);

	if (fields.length !== 0) {

		/** 
		 * Dump this condition into it's own function
		 * draw_charts
		 */

		var id = fields[0].get('ID');
		//fieldStats = field_info[id];

		// Should pass the chart #Container, Data
		// Break into two passes -> prepare -> draw
		draw_highchart(field_info[id]['1000m'].abundance, '#1km-abund', abundanceLegend, '1000yd');
		draw_highchart(field_info[id]['1000m'].landcover, '#1km-land', landCoverLegend, '1000yd');
		draw_highchart(field_info[id]['250m'].abundance, '#250m-abund', abundanceLegend, '250yd');
		draw_highchart(field_info[id]['250m'].landcover, '#250m-land', landCoverLegend, '250yd');

		$('#info').toggle(true);

		// Hide the popup
		overlay.setPosition(undefined);
	}
	else {

		/** 
		 * This block deals with giving the stats at clicked position
		 */

		var viewResolution = /** @type {number} */ (mapView.getResolution());
		var addr = wms_sources[1].getGetFeatureInfoUrl( evt.coordinate, viewResolution, 
			'EPSG:3857', {'INFO_FORMAT': 'text/xml'});
		// Get info from the LandCover layer as well
		url = addr.replace('OverallAbund', 'OverallAbund,LandCover');

		if (url) {
			$.ajax({
				url: url,
				dataType: 'xml',
				success: function(result) {
						var values = [];

						$(result).find("Attribute").each( function () {
							values.push(parseFloat($(this).attr('value')));
						});

						$('#info').toggle(false);

						popupTable = preparePopup(values);
						content.innerHTML = '';
						content.appendChild(popupTable);
						overlay.setPosition(evt.coordinate);
				}
			});
		}
	}
}


/**
 * Clear all selected features on map
 */
function clear_map_features () {

	for (var i = selected_features.length - 1; i >= 0; i--) {
		if (selected_features[i].get("IS_FIELD") == "1") {
			selected_features[i].setStyle(fieldStyle);	
		}
		else {
			selected_features[i].setStyle(clearStyle);
		}
	}

	// Remove all references from global array
	selected_features.length = 0;
}

/**
 * Color all items in selected fields
 */
function highlight_map_features () {


	for (var i = selected_features.length - 1; i >= 0; i--) {
		if (selected_features[i].get("IS_FIELD") == "1") {
			selected_features[i].setStyle(nearStyle);	
		}
		else {
			selected_features[i].setStyle(farStyle);
		}
	}
}

/**
 * Highlight selected field objects
 * fields: Array
 */
function mark_map_features (fields) {

	var farBuff = farSource.getFeatures();
	var nearBuff = nearSource.getFeatures();

	// Only do something if clicked field isn't already selected
	if (selected_features.indexOf(fields[0]) == -1) {

		// If clicking on different field clear everything else
		clear_map_features();
		selected_features.push(fields[0]);

		// Find outer buffer for field
		for (var i = 0, len = farBuff.length; i < len; i++) {
			if (farBuff[i].get('ID') == fields[0].get("ID")) {
				selected_features.push(farBuff[i]);
				break;
			}
		}

		// Find inner buffer for field
		for (i = 0, len = nearBuff.length; i < len; i++) {
			if (nearBuff[i].get('ID') == fields[0].get("ID")) {
				selected_features.push(nearBuff[i]);
				break;
			}
		}
	}
}

/**
 * Highlight or deselect objects on map
 */
function color_features (fields) {

	// Select/Deselect fields on map
	if (fields.length === 0) {
		clear_map_features();
	}
	else {
		mark_map_features(fields);
	}

	// Highlight currently selected items on map
	highlight_map_features();
}

function draw_highchart(data, container, legend, title) {
	
	// Keep consistent indexes between QGIS and here
	translator = {
		"Agriculture Field": 6, 
		"Blueberries": 7, 
		"Coniferous Forest": 2, 
		"Deciduous Forest": 3, 
		"Deciduous/Mixed Forest Edge": 0, 
		"Developed": 1, 
		"Emergent Wetlands": 4, 
		"Wetlands": 5,
		"High": 4, 
		"Low": 0, 
		"Low/Medium": 1, 
		"Medium": 2, 
		"Medium/High": 3 
	};

	var chart_data = [];
	for (var key in data) {
		if (data.hasOwnProperty(key)) {
			var record = {};
			record.name = key;
			record.y = data[key];
			record.color = legend[translator[key]][1]; 
			chart_data.push(record);
		}
	}

	prepare_highchart(chart_data, title, container);
}

function preparePopup(values) {

	var data = {
		'abundance': translateDensity(values[0]), 		
		'landcover': translateLand(values[1]),
	};
	var template = $('#popup-box').html();
	var popupTable = document.createElement('table');
	popupTable.innerHTML = Mustache.render(template, data);
	return popupTable;
}

function prepare_highchart(data, title, container) {

	$(container).highcharts({
		chart: {
			margin: [0, 0, 0, 0],
			spacingTop: 0,
			spacingBottom: 0,
			spacingLeft: 0,
			spacingRight: 0,
			plotBackgroundColor: null,
			backgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false
		},
		credits: {
			enabled: false,
		},
		title: {
			text: title,
		},
		plotOptions: {
			pie: {
				size: '90%',
				slicedOffset: 10,
				//startAngle: -90,
				//endAngle: 90,
				allowPointSelect: true,
				tooltip: {
					pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>'
				},
				dataLabels: {
					enabled: false,
					format: '<b>{point.name}</b>',
					style: {
						color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
					}
				},
				showInLegend: false,
			}
		},
		series: [{
			type: 'pie',
			name: 'Type',
			//innerSize: '50%',
			data: data		
		}]
	});
}

function prepareLayerToggle() {

	var table = document.createElement('table');
	table.className = 'layerTable';
	var tbody = document.createElement('tbody');
	var outerDiv = document.getElementById('layerToggles');

	var no_display = ['Sat', 'County Labels', 'Town Labels', 'Waterbody Labels', 'OverallAbund', 'LandCover', 'Road'];

	toggle_layers = wms_layers.concat(layers[2]);

	for (var i = 0; i < toggle_layers.length; i++) {

		var layerName = toggle_layers[i].get('title');

		if (no_display.indexOf(layerName) == -1) {
			var checkBox = document.createElement('input');
			checkBox.type = 'checkbox';
			checkBox.className = 'layerToggle';
			checkBox.name = i;
			checkBox.id = 'layer' + i;
			checkBox.checked = toggle_layers[i].getVisible();

			var row = document.createElement('tr');
			var cellLeft = document.createElement('td');
			cellLeft.appendChild(document.createTextNode(layerName));

			var cellRight = document.createElement('td');
			cellRight.className = 'right';
			cellRight.appendChild(checkBox);

			row.appendChild(cellLeft);
			row.appendChild(cellRight);
			tbody.appendChild(row);
		}
	}
	
	table.appendChild(tbody);
	outerDiv.appendChild(table);
} 
prepareLayerToggle();
addLegends();

$('.layerToggle').change( function() {
	var layerIndex = parseInt($(this).attr('name'));
	toggle_layers[layerIndex].setVisible(this.checked);
});


$('#LayerToggleButton').click( function() {
	$('#layerBox').slideToggle();
});

$('#infoClose').click( function(){
	//selection.getFeatures().clear();
	clear_map_features();
	$(this).parent().toggle(false);
});

// TODO Set view level on center map
$('#searchButton').click( function() {
	var view = map.getView();
	var coords = $('#searchInput').val().split(',');
	var address = $('#searchInput').val();
	if (address !== "") {
		if (address.match(/[a-z]/i)) {
			$.ajax({
				url : "https://maps.googleapis.com/maps/api/geocode/json?address="+$('#searchInput').val()+"&sensor=false",
				method: "POST",
				success:function(data){
					coords = [];
					coords[0] = parseFloat(data.results[0].geometry.location.lng);
					coords[1] = parseFloat(data.results[0].geometry.location.lat);
					var center = ol.proj.transform([coords[0], coords[1]], 'EPSG:4326', 'EPSG:3857');
					view.setCenter(center);
					view.setZoom(15);
				}
			});
		}
		else if ($('#searchInput').val().indexOf(',') == -1) {
			var zipcode = coords;
			$.ajax({
				url : "https://maps.googleapis.com/maps/api/geocode/json?address=united+states&components=postal_code:"+zipcode+"&sensor=false",
				method: "POST",
				success:function(data){
					coords = [];
					coords[0] = parseFloat(data.results[0].geometry.location.lng);
					coords[1] = parseFloat(data.results[0].geometry.location.lat);
					var center = ol.proj.transform([coords[0], coords[1]], 'EPSG:4326', 'EPSG:3857');
					view.setCenter(center);
					view.setZoom(13);
				}
			});
		}
		else {
			coords[0] = parseFloat(coords[0]);
			coords[1] = parseFloat(coords[1]);
			var center = ol.proj.transform([coords[1], coords[0]], 'EPSG:4326', 'EPSG:3857');
			view.setCenter(center);
			view.setZoom(15);
		}
	}
});

$("#fieldToggle").click( function(){
	var fields = fieldSource.getFeatures();
	if (fields_visible) {
		var changeStyle = clearStyle;
		fields_visible = false;
	}
	else {
		var changeStyle = fieldStyle;
		fields_visible = true;
	}

	for (var i = 0; i < fields.length; i++) {
		fields[i].setStyle(changeStyle);
	}
});

function addLegends () {

	$('#legendMap').append(drawLegend(landCoverLegend, 'Land Cover', 'lc'));
	$('#legendMap').append(drawLegend(abundanceLegend, 'Abundance', 'ab'));
	$('.tipster').tooltipster({theme: 'tooltipster-shadow'});
}


$('#abundanceToggleButton').click(function () {
	$('#abundanceToggleButton').toggleClass('selected');
	$('#abundanceSliderDiv').slideToggle('slow', function () {
		$('#ab').slideToggle('slow', function () {
			$("#legendBox").toggle(true);
		});
		abundance_layer.setVisible(!abundance_layer.getVisible());
	});
});

$('#landToggleButton').click(function () {
	$('#landToggleButton').toggleClass('selected');
	$('#landSliderDiv').slideToggle('slow', function() {
		$('#lc').slideToggle('slow', function () {
			$("#legendBox").toggle(true);
		});
		landcover_layer.setVisible(!landcover_layer.getVisible());
	});
});

$('#satToggleButton').click(function () {
	satellite_layer.setVisible(!satellite_layer.getVisible());
	$('#satToggleButton').toggleClass('selected');
	road_layer.setVisible(!road_layer.getVisible());
	$('#roadToggleButton').toggleClass('selected');
});

$('#roadToggleButton').click(function () {
	road_layer.setVisible(!road_layer.getVisible());
	$('#roadToggleButton').toggleClass('selected');
	satellite_layer.setVisible(!satellite_layer.getVisible());
	$('#satToggleButton').toggleClass('selected');
});

$('.layerSlider').change( function() {
	var layerIndex = parseInt($(this).attr('name'));
	var value = $(this).val();
	wms_layers[layerIndex].setOpacity(value);
});

$("#legend").click( function(){
	$("#legendBox").slideToggle();
});

$('#ZoomExtent').click( function() {
	view = map.getView();
	view.setCenter([-7595180.242828147,5558499.882142702]);
	view.setZoom(10);
});

$("#searchInput").keydown(function(event){
	if(event.keyCode == 13){
		$("#searchButton").click();
	}
});

$(".ol-attribution button").click( function () {
	// Give them credit for their hard work elsewhere
	$(".ol-attribution ul").remove();
	$("#feedback-form").toggle();
});

function handleSelection () {
	if (drawing) {
		map.removeInteraction(draw);
		//map.on('singleclick', interact);
		map.on('singleclick', map_click);
		$(this).html('Select');
		drawing = false;
	}
	else {
		map.addInteraction(draw);
		//map.un('singleclick', interact);
		map.un('singleclick', map_click);
		$(this).html('Done');
		drawing = true;
	}
}

$("#select-button").click(handleSelection);

$("#clear-select").click( function () {
    source_interaction.clear(true);
	//source_interaction.forEachFeature( function (fet) {
	//	source_interaction.removeFeature(fet);
	//});
});

function extractPoints () {
	var features = source_interaction.getFeatures();
	if (features.length === 0) {
		return ' ';
	}
	else {
		var points = features[0].getGeometry().getExtent();
		return points.toString();
	}
}

function extractForm () {
	var c = $('#problem').val();
	var n = $('#name').val();
	var e = $('#email').val();
	var f = $('#sugs').val();
	var p = extractPoints();

	return {category:c, name:n, email:e, feedback:f, polygon:p};
}

$("#form-submit").click( function () {
	map.removeInteraction(draw);
	//map.on('singleclick', interact);
	$('#selectButton').html('Select');
	drawing = false;

	var form_data = extractForm();
	$.ajax({
		url: '/php/submission.php',
		data: form_data,
		dataType: 'text',
		success: function(result) {
			if (result != "Success") {
				alert('Email did not send, please check all fields are entered!');
			}
			else {
				alert('Email sent successfully!');
				$("#feedback-form").toggle();
			}
		}
	});

});

$("#close-submit").click( function () {
	$("#feedback-form").toggle();

	map.removeInteraction(draw);
	map.on('singleclick', map_click);
	$('#selectButton').html('Select');
	drawing = false;
});

// Trigger actions on map with mouse click
map.on('singleclick', map_click); 

// Trigger mouse pointer to 'clicker' on clickable elements
var target = map.getTarget();
var jTarget = typeof target === "string" ? $("#"+target) : $(target);


map.on("pointermove", function (event) {
    var mouseCoordInMapPixels = [event.originalEvent.offsetX, event.originalEvent.offsetY];

    // Only return if pointer over field
    var hit = map.forEachFeatureAtPixel(mouseCoordInMapPixels, function (feature, layer) {
        if (layer.get("title") == "Fields") {
            return true;
        }
    });

    if (hit) {
        jTarget.css("cursor", "pointer");
    } else {
        jTarget.css("cursor", "");
    }
});
