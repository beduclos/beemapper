module.exports = function(grunt) {

  // Project configuration.
grunt.initConfig({
	pkg: grunt.file.readJSON('package.json'),
	uglify: {
		options: {
		banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
		},
		build: {
			src: 'js/**/*.js',
			dest: 'build/<%= pkg.name %>.min.js'
		}
	},
	concat: {
		options: {
			separator: ';'
		},
		dist: {
			src: ['js/**/*.js'],
			dest: 'dist/<%= pkg.name %>.js'
		}
	},
});

// Load the plugin that provides the "uglify" task.
grunt.loadNpmTasks('grunt-contrib-concat');
grunt.loadNpmTasks('grunt-contrib-uglify');


// Default task(s).
grunt.registerTask('default', ['concat', 'uglify']);

};
