import json
import xmltodict
import argparse
import glob
import os



def get_files(directory):

    full_path = os.path.abspath(directory)
    files = glob.glob(full_path + '/*.tif.aux.xml')

    return files

def extract_landcover(files, buff, skeleton):

    for f in files:

        #print '[Processing] ', f

        path, fname = os.path.split(f)
        name, _, _, _ = fname.split('.')
        xml = xmltodict.parse(open(f, 'r').read())
        fields = xml['PAMDataset']['PAMRasterBand']['GDALRasterAttributeTable']['Row']

        for field in fields:

            val = int(field['F'][2])

            if field['F'][3] == 'Deciduous/mixed forest edge':
                skeleton[name][buff]['landcover']['Deciduous/Mixed Forest Edge'] = val     

            elif field['F'][3] == 'Deciduous/mixed forest':
                skeleton[name][buff]['landcover']['Deciduous Forest'] = val

            elif field['F'][3] == 'Developed/other':
                skeleton[name][buff]['landcover']['Developed'] = val       

            elif field['F'][3] == 'Agriculture/grass':
                skeleton[name][buff]['landcover']['Agriculture Field'] = val        

            elif field['F'][3] == 'Coniferous forest':
                skeleton[name][buff]['landcover']['Coniferous Forest'] = val        

            elif field['F'][3] == 'Emergent wetland/scrub-shrub':
                skeleton[name][buff]['landcover']['Emergent Wetlands'] = val        

            elif field['F'][3] == 'Wetlands/water':
                skeleton[name][buff]['landcover']['Wetlands'] = val  

            elif field['F'][3] == 'Blueberries':
                skeleton[name][buff]['landcover']['Blueberries'] = val

            else:

                print "Missed a value, doublecheck!"
                exit(1)

        # Get some nice percentages now!
        values = skeleton[name][buff]['landcover']
        total = 0
        for key in values:
            total += values[key]

        for key in values:
            values[key] = round(values[key] / float(total), 2)

def extract_abundance(files, buff, skeleton):

    for f in files:

        #print '[Processing] ', f

        path, fname = os.path.split(f)
        name, _, _, _ = fname.split('.')
        xml = xmltodict.parse(open(f, 'r').read())
        fields = xml['PAMDataset']['PAMRasterBand']['GDALRasterAttributeTable']['Row']

        for field in fields:

            if field['F'][1] == '1':
                skeleton[name][buff]['abundance']['Low'] = int(field['F'][2])        
            elif field['F'][1] == '2':
                skeleton[name][buff]['abundance']['Low/Medium'] = int(field['F'][2])        
            elif field['F'][1] == '3':
                skeleton[name][buff]['abundance']['Medium'] = int(field['F'][2])        
            elif field['F'][1] == '4':
                skeleton[name][buff]['abundance']['Medium/High'] = int(field['F'][2])        
            elif field['F'][1] == '5':
                skeleton[name][buff]['abundance']['High'] = int(field['F'][2])        
            else:
                print "Missed a value!"
                exit(1)

        # Get some nice percentages now!
        values = skeleton[name][buff]['abundance']
        total = 0
        for key in values:
            total += values[key]

        for key in values:
            values[key] = round(values[key] / float(total), 2)




if __name__ == '__main__':

    skeleton = {}

    for i in range(1, 1274):
        skeleton[str(i)] = {"1000m": {"abundance": {"High": 0, "Low/Medium": 0, "Medium": 0, "Low": 0, "Medium/High": 0}, 
                                "landcover": {"Emergent Wetlands": 0, "Wetlands": 0, "Blueberries": 0, "Developed": 0, "Agriculture Field": 0, "Deciduous/Mixed Forest Edge": 0, "Coniferous Forest": 0, "Deciduous Forest": 0}}, 
                        "250m": {"abundance": {"High": 0, "Low/Medium": 0, "Medium": 0, "Low": 0, "Medium/High": 0}, 
                                "landcover": {"Emergent Wetlands": 0, "Wetlands": 0, "Blueberries": 0, "Developed": 0, "Agriculture Field": 0, "Deciduous/Mixed Forest Edge": 0, "Coniferous Forest": 0, "Deciduous Forest": 0}}}


    # 1K Raster Summary
    files = get_files('/home/rpskillet/staging/launch_data/abundancerasters1k')
    extract_abundance(files, '1000m', skeleton)

    # 250 Raster Summary
    files = get_files('/home/rpskillet/staging/launch_data/abundancerasters250')
    extract_abundance(files, '250m', skeleton)

    # 1K Land Cover Summary
    files = get_files('/home/rpskillet/staging/launch_data/landcoverrasters1k')
    extract_landcover(files, '1000m', skeleton)

    # 250 Land cover Summary
    files = get_files('/home/rpskillet/staging/launch_data/landcoverrasters250')
    extract_landcover(files, '250m', skeleton)

    print json.dumps(skeleton)




