import os
import json

data = {}
with open('backup_all.json', 'r') as field_file:
    ff = json.load(field_file)

    with open('compiled_250m.json', 'r') as buff_file:
        bf = json.load(buff_file)

        data = ff
        for key in data.keys():
            data[key]['250m']['abundance'] = bf[key]['250m']['abundance']


with open('final_all.json' , 'w') as out_file:
    json.dump(data, out_file, sort_keys = True, indent = 4)
