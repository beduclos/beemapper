// Define a search box on the map
var searchBox = function(opt_options) {
	var options = opt_options || {};
	var data = {};
	var template = $('#search-template').html();
	var element = document.createElement('div');
	element.innerHTML = Mustache.render(template, data);

	ol.control.Control.call(this, {
		element: element,
		target: options.target
	});
};
ol.inherits(searchBox, ol.control.Control);

// Define a search box on the map
var feedBack = function(opt_options) {
	var options = opt_options || {};
	var data = {};
	var template = $('#feedback').html();
	var element = document.createElement('div');
	element.innerHTML = Mustache.render(template, data);


	ol.control.Control.call(this, {
		element: element,
		target: options.target
	});
};
ol.inherits(feedBack, ol.control.Control);

// Define a box on the map to hold area info text
var info = function(opt_options) {
	var options = opt_options || {};
	var data = {};

	var template = $('#info-box').html();
	var element = document.createElement('div');
	element.innerHTML = Mustache.render(template, data);

	ol.control.Control.call(this, {
		element: element,
		target: options.target
	});
};
ol.inherits(info, ol.control.Control);

// Define a button on the map to toggle the Legend
var legendToggle = function(opt_options) {
	var options = opt_options || {};
	var data = {
		'div-id': 'legend',
		'b-title': 'Click to show legend',
		'b-class': 'toggle',
		'b-id': '',
		'text': 'Legend'
	};
	var template = $('#map-button').html();
	var element = document.createElement('div');
	element.innerHTML = Mustache.render(template, data);

	ol.control.Control.call(this, {
		element: element,
		target: options.target
	});
};

ol.inherits(legendToggle, ol.control.Control);

// Define a box on the map to hold area legend image
var legendBox = function(opt_options) {
	var options = opt_options || {};
	var data = {
		'd-class': 'ol-legendControl',
		'd-id': 'legendBox',
		'dd-id': 'legendMap',
	};
	var template = $('#map-box').html();
	var element = document.createElement('div');
	element.innerHTML = Mustache.render(template, data);

	ol.control.Control.call(this, {
		element: element,
		target: options.target
	});
};

ol.inherits(legendBox, ol.control.Control);

// Define a box on the map to hold area legend image
var layerBox = function(opt_options) {
	var options = opt_options || {};
	var data = {
		'd-class': 'ol-layerBox',
		'd-id': 'layerBox',
		'dd-id': 'layerToggles',
	};
	var template = $('#map-box').html();
	var element = document.createElement('div');
	element.innerHTML = Mustache.render(template, data);

	ol.control.Control.call(this, {
		element: element,
		target: options.target
	});
};
ol.inherits(layerBox, ol.control.Control);

var LayerToggle = function(opt_options) {
	var options = opt_options || {};
	var data = {
		'div-id': 'LayerToggleDiv',
		'b-title': 'Show additional layers',
		'b-class': '',
		'b-id': 'LayerToggleButton',
		'text': 'Layers'
	};
	var template = $('#map-button').html();
	var element = document.createElement('div');
	element.innerHTML = Mustache.render(template, data);

	ol.control.Control.call(this, {
		element: element,
		target: options.target
	});
};
ol.inherits(LayerToggle, ol.control.Control);

var abundanceToggle = function(opt_options) {
	var options = opt_options || {};
	var data = {
		'div-id': 'AbundanceToggle',
		'div-value': 1,
		'b-title': 'Toggle abundance layer',
		'b-id': 'abundanceToggleButton',
		'b-val': 1,
		'text': 'Abundance',
		'idiv-class': 'sliderDiv',
		'idiv-id': 'abundanceSliderDiv',
		'input-title': 'Adjust Map Transparency',
		'input-class': 'layerSlider',
		'input-name': '5',
	};
	var template = $('#layer-toggle').html();
	var element = document.createElement('div');
	element.innerHTML = Mustache.render(template, data);

	ol.control.Control.call(this, {
		element: element,
		target: options.target
	});
};
ol.inherits(abundanceToggle, ol.control.Control);

var landToggle = function(opt_options) {
	var options = opt_options || {};
	var data = {
		'div-id': 'LandToggle',
		'div-value': 1,
		'b-title': 'Toggle land cover layer',
		'b-id': 'landToggleButton',
		'b-val': 1,
		'text': 'Land Cover',
		'idiv-class': 'sliderDiv',
		'idiv-id': 'landSliderDiv',
		'input-title': 'Adjust Map Transparency',
		'input-class': 'layerSlider',
		'input-name': '4',
	};
	var template = $('#layer-toggle').html();
	var element = document.createElement('div');
	element.innerHTML = Mustache.render(template, data);

	ol.control.Control.call(this, {
		element: element,
		target: options.target
	});
};
ol.inherits(landToggle, ol.control.Control);

var satToggle = function(opt_options) {
	var options = opt_options || {};
	var data = {
		'div-id': 'satToggle',
		'b-title': 'Toggle satellite base layer',
		'b-class': '',
		'b-id': 'satToggleButton',
		'text': 'Satellite'
	};
	var template = $('#map-button').html();
	var element = document.createElement('div');
	element.innerHTML = Mustache.render(template, data);

	ol.control.Control.call(this, {
		element: element,
		target: options.target
	});
};
ol.inherits(satToggle, ol.control.Control);

var roadToggle = function(opt_options) {
	var options = opt_options || {};
	var data = {
		'div-id': 'roadToggle',
		'b-title': 'Toggle road base layer',
		'b-class': '',
		'b-id': 'roadToggleButton',
		'text': 'Road'
	};
	var template = $('#map-button').html();
	var element = document.createElement('div');
	element.innerHTML = Mustache.render(template, data);

	ol.control.Control.call(this, {
		element: element,
		target: options.target
	});
};
ol.inherits(roadToggle, ol.control.Control);

var ZoomToExtent = function(opt_options) {
	var options = opt_options || {};
	var data = {
		'div-id': 'ZoomToExtent',
		'b-title': 'Zoom to map extent',
		'b-class': '',
		'b-id': 'ZoomExtent',
		'text': 'R'
	};
	var template = $('#map-button').html();
	var element = document.createElement('div');
	element.innerHTML = Mustache.render(template, data);

	ol.control.Control.call(this, {
		element: element,
		target: options.target
	});
};

ol.inherits(ZoomToExtent, ol.control.ZoomToExtent);

// Setup the map object
var mapView = new ol.View({
	center: [-7595180.242828147,5558499.882142702],
	zoom: 10,
	minZoom: 9,
	maxZoom: 17 
});

var container = document.getElementById('popup');
var content = document.getElementById('popup-content');
var closer = document.getElementById('popup-closer');

closer.onclick = function() {
	overlay.setPosition(undefined);
	closer.blur();
	return false;
};

var overlay = new ol.Overlay({
	element: container
});

// Actually create the map
var map = new ol.Map({
	controls: ol.control.defaults({
	}).extend([
		new info(),
		new LayerToggle(),
		new layerBox(),
		new legendToggle(),
		new legendBox(),
		new searchBox(),
		new ZoomToExtent(),
		new abundanceToggle(),
		new landToggle(),
		new roadToggle(),
		new satToggle(),
		new ol.control.ScaleLine({'units': 'us'}),
		new feedBack(),
	]),
	layers: wms_layers.concat(layers),
	target: 'map',
	overlays: [overlay],
	view: mapView
});
