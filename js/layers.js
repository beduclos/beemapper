// Handle the layer creation and stuff
var wms_sources = [];
var wms_layers = [];
var toggle_layers = [];
var layers = [];

// WMS sources
var sources = [
	'LandCover',
	'OverallAbund',
	'LandCover',
	'OverallAbund',
	'Lakes',
	'Rivers / Streams',
	'Towns',
	'Roads',
];

// Define an invisible layer for highlighting missing data
var source_interaction = new ol.source.Vector();
var layer_interaction = new ol.layer.Vector({
	source: source_interaction,
	style: new ol.style.Style({
		fill: new ol.style.Fill({
			color: 'rgba(150, 150, 150, 0.2)'
		}),
		stroke: new ol.style.Stroke({
			color: '#ffcc33',
			width: 2
		}),
		image: new ol.style.Circle({
			radius: 7,
			fill: new ol.style.Fill({
				color: '#ffcc33'
			})
		})
	})
});

// Define sources for the shape files
var fieldSource = new ol.source.Vector({
	url: './maps/fields.json',
	format: new ol.format.GeoJSON()
});

var nearSource = new ol.source.Vector({
	url: './maps/250Buffer.json',
	format: new ol.format.GeoJSON()
});

var farSource = new ol.source.Vector({
	url: './maps/1KBuffer.json',
	format: new ol.format.GeoJSON()
});

// Define styles for the vector layers
var clearStyle = new ol.style.Style({
	fill: new ol.style.Fill({
		color: 'rgba(0,0,0,0)',
	})
});

var fieldStyle = new ol.style.Style({
	fill: new ol.style.Fill({
			color: 'rgba(0, 0, 240, 0.8)',
	})
});

var mouseStyle = new ol.style.Style({
    fill: new ol.style.Fill({
        color: 'rgba(255,179,0, 0.8)',
    }),
});

var nearStyle = [new ol.style.Style({
	fill: new ol.style.Fill({
			color:'rgba(0,0,120,0.05)', 
	}),
	stroke: new ol.style.Stroke({color: 'black', width: 1})
})];

var farStyle = [new ol.style.Style({
	fill: new ol.style.Fill({
			color:'rgba(0,0,120,0.1)', 
			opacity: 0.75
	}),
	stroke: new ol.style.Stroke({color: 'black', width: 1})
})];

wms_layers.push(new ol.layer.Tile({
	title: 'Sat',
	visible: false,
	preload: Infinity,
	source: new ol.source.BingMaps({
		key: 'AqRY7vLav9l1mIc1iGuVDZEw6Ju6BFS8YSrL6haLdYin7nmooM6hlH_uKQHB5XmV',
		imagerySet: ['Aerial'], 
		maxZoom: 16
	}),
}));

wms_layers.push(new ol.layer.Tile({
	title: 'Road',
	visible: true,
	preload: Infinity,
	source: new ol.source.BingMaps({
		key: 'AqRY7vLav9l1mIc1iGuVDZEw6Ju6BFS8YSrL6haLdYin7nmooM6hlH_uKQHB5XmV',
		imagerySet: ['Road'], 
		maxZoom: 16
	}),
}));

// Add the raster data from WMS
for (var i = 0, len = sources.length; i < len; i++) {

	var url = 'https://beemapper-wfcb.acg.umaine.edu/mapproxy/service?';
	var format = 'image/png';

	if (i >= 2) {
		wms_sources.push(
			new ol.source.TileWMS({
				url: 'https://beemapper-wfcb.acg.umaine.edu/mapproxy/service',
				params: {
					LAYERS: sources[i],
					VERSION: '1.1.1'
				}
			})
		);
		wms_layers.push(
			new ol.layer.Tile({
				source: wms_sources[i],
				title: sources[i],
				preload: Infinity,
			})
		);

	}
	else {

		wms_sources.push(
			new ol.source.ImageWMS({
					url: url,
					params: {
						'LAYERS': sources[i],
						'FORMAT': format,
					}
			})
		);

		wms_layers.push(
			new ol.layer.Image({
					title: sources[i],
					preload: Infinity,
					source: wms_sources[i],
			})
		);
	}


	// Don't make the Abund visible to start
	if (i == 2 || i == 3 | i == 4 | i == 5) {
		wms_layers[i].setVisible(false);
	}
}

var satellite_layer = wms_layers[0];
var road_layer = wms_layers[1];
var landcover_layer = wms_layers[4];
var abundance_layer = wms_layers[5];

layers.push(
		new ol.layer.Vector({
			title: '1000M',
			style: clearStyle,
			source: farSource
		})
);

layers.push(
		new ol.layer.Vector({
			title: '250M',
			style: clearStyle,			
			source: nearSource
	})
);

layers.push(
		new ol.layer.Vector({
			title: 'Fields',
			style: fieldStyle,
			source: fieldSource
		})
);

layers.push( layer_interaction );
