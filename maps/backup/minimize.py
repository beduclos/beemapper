import json

data = {}
with open('fields.json', 'r') as f:
    data = json.load(f)

for index, field in enumerate(data['features']):
    for cin, coord in enumerate(field['geometry']['coordinates']):
        for nim, number in enumerate(coord):
            for lin, loc in enumerate(number):
                data['features'][index]['geometry']['coordinates'][cin][nim][lin] = round(loc, 5)

with open('fields_smaller.json', 'w') as out:
    json.dump(data, out, sort_keys=True, indent=0)
